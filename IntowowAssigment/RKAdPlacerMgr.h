//
//  RKAdPlacer.h
//  IntowowAssigment
//
//  Created by FDT14009Mac on 2016/9/19.
//  Copyright © 2016年 FDT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import FBAudienceNetwork;

@interface RKAdPlacerMgr : NSObject

/**
 *  Create and return an ad placer manager to handle placer's call back
 *
 *  @param beginIndexPath   Index of first ad.
 *  @param endIndexPath     Index of last ad.
 *  @param shortestInterval Shortest interval between two ads.
 *  @param tableView        The table view in which to insert ads.
 *  @param viewController   The view controller which should be used to present modal content.
 *
 *  @return An `RKAdPlacerMgr` object.
 */
-(nonnull instancetype)initWithBeginIndexPath:(nonnull NSIndexPath *)beginIndexPath
                                 endIndexPath:(nonnull NSIndexPath *)endIndexPath
                             shortestInterval:(NSTimeInterval)shortestInterval
                                    tableView:(nonnull UITableView *)tableView
                               viewController:(nonnull UIViewController *)viewController;

//@property (strong, nonatomic) FBNativeAdTableViewCellProvider *ads;
@end
