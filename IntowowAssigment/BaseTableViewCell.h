//
//  BaseTableViewCell.h
//  IntowowAssigment
//
//  Created by FDT14009Mac on 2016/9/19.
//  Copyright © 2016年 FDT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MPTableViewAdPlacerView.h"
@interface BaseTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet MPTableViewAdPlacerView *view;

@end
