//
//  RKAdPlacer.m
//  IntowowAssigment
//
//  Created by FDT14009Mac on 2016/9/19.
//  Copyright © 2016年 FDT. All rights reserved.
//

#import "RKAdPlacerMgr.h"
#import <Fabric/Fabric.h>
#import "MoPub.h"
#import "MPTableViewAdPlacer.h"
#import "MPTableViewAdPlacerView.h"
@import FBAudienceNetwork;

@interface RKAdPlacerMgr() <MPTableViewAdPlacerDelegate>

@property (nonatomic, strong) MPTableViewAdPlacer *placer;
@property (nonatomic, strong) MPClientAdPositioning *positioning;

@end

@implementation RKAdPlacerMgr

-(nonnull instancetype)initWithBeginIndexPath:(nonnull NSIndexPath *)beginIndexPath
                                 endIndexPath:(nonnull NSIndexPath *)endIndexPath
                             shortestInterval:(NSTimeInterval)shortestInterval
                                    tableView:(nonnull UITableView *)tableView
                               viewController:(nonnull UIViewController *)viewController {
    
    self = [super init];
    if (self) {
        
        // Create a targeting object to serve better ads.
        MPNativeAdRequestTargeting *targeting = [MPNativeAdRequestTargeting targeting];
        targeting.location = [[CLLocation alloc] initWithLatitude:37.7793 longitude:-122.4175];
        targeting.desiredAssets = [NSSet setWithObjects:kAdIconImageKey, kAdMainImageKey, kAdCTATextKey, kAdTextKey, kAdTitleKey, nil];
        
        MPStaticNativeAdRendererSettings *nativeAdSettings = [[MPStaticNativeAdRendererSettings alloc] init];
        nativeAdSettings.renderingViewClass = [MPTableViewAdPlacerView class];
        nativeAdSettings.viewSizeHandler = ^(CGFloat maximumWidth) {
            return CGSizeMake(maximumWidth, 312.0f);
        };
        MPNativeAdRendererConfiguration *nativeAdConfig = [MPStaticNativeAdRenderer rendererConfigurationWithRendererSettings:nativeAdSettings];
        
        /**
         *  Positioning logic
         */
        [self setUpPositioningWithBeginIndexPath:beginIndexPath
                                    endIndexPath:endIndexPath
                                shortestInterval:shortestInterval];
        
        self.placer = [MPTableViewAdPlacer placerWithTableView:tableView
                                                viewController:viewController
                                                 adPositioning:self.positioning
                                        rendererConfigurations:@[nativeAdConfig]];
        
        self.placer.delegate = self;
        [self.placer loadAdsForAdUnitID:@"c8f013bbcbb947519671dcdd28e4a430" targeting:targeting];
    }
    return self;
}

- (void) setUpPositioningWithBeginIndexPath:(nonnull NSIndexPath *)beginIndexPath
                               endIndexPath:(nonnull NSIndexPath *)endIndexPath
                           shortestInterval:(NSTimeInterval)shortestInterval{
    
    self.positioning = [MPClientAdPositioning positioning];
    [self.positioning addFixedIndexPath:beginIndexPath];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:beginIndexPath.row + shortestInterval + arc4random()%5 inSection:0];
    
    // 兩則廣告之間最少間隔多少位置
    while (indexPath.row <= endIndexPath.row) {
        [self.positioning addFixedIndexPath:indexPath];
        indexPath = [NSIndexPath indexPathForRow:indexPath.row + shortestInterval + arc4random()%5 inSection:0];
    }
}

#pragma mark - UITableViewAdPlacerDelegate

- (void) nativeAdWillPresentModalForTableViewAdPlacer:(MPTableViewAdPlacer *)placer
{
    NSLog(@"Table view ad placer will present modal.");
}

- (void) nativeAdDidDismissModalForTableViewAdPlacer:(MPTableViewAdPlacer *)placer
{
    NSLog(@"Table view ad placer did dismiss modal.");
}

- (void) nativeAdWillLeaveApplicationFromTableViewAdPlacer:(MPTableViewAdPlacer *)placer
{
    NSLog(@"Table view ad placer will leave application.");
}
@end
