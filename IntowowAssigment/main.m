//
//  main.m
//  IntowowAssigment
//
//  Created by FDT14009Mac on 2016/9/19.
//  Copyright © 2016年 FDT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
