//
//  ViewController.m
//  IntowowAssigment
//
//  Created by FDT14009Mac on 2016/9/19.
//  Copyright © 2016年 FDT. All rights reserved.
//

#import "ViewController.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "MPTableViewAdPlacer.h"
#import "BaseTableViewCell.h"
#import "RKAdPlacerMgr.h"

static NSString *kDefaultCellIdentifier = @"MoPubSampleAppTableViewAdPlacerCell";
static NSString *kDefaultCellContentImageURLString = @"https://static.juksy.com/files/articles/56063/57a1721953fa2.jpg";
static NSString *kDefaultCellIconImageURLString = @"https://lh3.googleusercontent.com/e32QLv_H4fsEE9ztnxkbRFQogySH-vMp4MiIxp_NC4tpFiPLB2vfNHrpGqeGY0Lyzg=w170";

@interface ViewController ()
@property (nonatomic, strong) NSMutableArray *contentItems;
@property (nonatomic, strong) RKAdPlacerMgr *placerMgr;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = false;
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= MP_IOS_7_0
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
#endif
    [self setupContent];
    [self setupAdPlacer];
}
#pragma mark - Content

- (void)setupContent
{
    self.contentItems = [NSMutableArray array];
    
    [self loadingMoreContent];
    
    [self.contentItems sortUsingSelector:@selector(compare:)];
}

- (void)loadingMoreContent{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        for (NSString *fontFamilyName in [UIFont familyNames]) {
            for (NSString *fontName in [UIFont fontNamesForFamilyName:fontFamilyName]) {
                [self.contentItems addObject:fontName];
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.tableView mp_reloadData];
        });
    });
}

#pragma mark - AdPlacer
- (void)setupAdPlacer
{
    self.placerMgr = [[RKAdPlacerMgr alloc]
                      initWithBeginIndexPath:[NSIndexPath indexPathForRow:5 inSection:0]
                      endIndexPath:[NSIndexPath indexPathForRow:100 inSection:0]
                      shortestInterval:10
                      tableView:self.tableView
                      viewController:self];
}

#pragma mark - ScrollView Delegate
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger count = [self.contentItems count];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BaseTableViewCell *cell = [tableView mp_dequeueReusableCellWithIdentifier:kDefaultCellIdentifier forIndexPath:indexPath];
    if (!cell) {
        cell = [[BaseTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kDefaultCellIdentifier];
    }
    
    /**
     *  Fill content
     */
    NSString *fontName = self.contentItems[indexPath.row];
    cell.view.titleLabel.text = fontName;
    [cell.view.mainImageView sd_setImageWithURL:[NSURL URLWithString:kDefaultCellContentImageURLString]];
    [cell.view.iconImageView sd_setImageWithURL:[NSURL URLWithString:kDefaultCellIconImageURLString]];
    
    /**
     *  Loading more
     */
    if (indexPath.row == self.contentItems.count - 10) {
        [self loadingMoreContent];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 312;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView mp_deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
